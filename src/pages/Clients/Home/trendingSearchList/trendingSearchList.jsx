import React from 'react'
import './trendingSearchList.css'

export default function TrendingSearchList() {
  return (
    <div className='section_trending_search_list'>
        <div className='trending_search_list'>
            <div className='trending_search_header'>
                <div className='trending_search_header_title'>
                    XU HUONG TIM KIEM
                </div>
                <div className='trending_search_show_more'>
                    Xem them
                </div>
            </div>
            <div className='trending_search_content'>

            </div>
        </div>
    </div>
  )
}

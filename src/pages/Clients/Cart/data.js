export const cartItem = [
  {
    id: 1,

    img: "https://cf.shopee.vn/file/3e580db475298f74211affea22d34aac_tn",
    itemName: "Nước hoa hồng diếp cá Dokudami Natural Skin Lotion 500ml",
    href: "https://shopee.vn/N%C6%B0%E1%BB%9Bc-hoa-h%E1%BB%93ng-di%E1%BA%BFp-c%C3%A1-Dokudami-Natural-Skin-Lotion-500ml-i.573427746.10365861446?xptdk=a1b8b001-4417-47f6-8e7f-98d2871f07b1",
    chosen: "White",
    price: 12000,
    warehouse: 1000,
    quantity: 1,
  },
  {
    id: 2,

    img: "https://cf.shopee.vn/file/3e580db475298f74211affea22d34aac_tn",
    itemName: "Nước hoa hồng diếp cá Dokudami Natural Skin Lotion 500ml",
    href: "https://shopee.vn/N%C6%B0%E1%BB%9Bc-hoa-h%E1%BB%93ng-di%E1%BA%BFp-c%C3%A1-Dokudami-Natural-Skin-Lotion-500ml-i.573427746.10365861446?xptdk=a1b8b001-4417-47f6-8e7f-98d2871f07b1",
    chosen: "White",
    price: 1284000,
    warehouse: 1000,
    quantity: 2,
  },
  {
    id: 3,

    img: "https://cf.shopee.vn/file/3e580db475298f74211affea22d34aac_tn",
    itemName: "Nước hoa hồng diếp cá Dokudami Natural Skin Lotion 500ml",
    href: "https://shopee.vn/N%C6%B0%E1%BB%9Bc-hoa-h%E1%BB%93ng-di%E1%BA%BFp-c%C3%A1-Dokudami-Natural-Skin-Lotion-500ml-i.573427746.10365861446?xptdk=a1b8b001-4417-47f6-8e7f-98d2871f07b1",
    chosen: "White",
    price: 12407800,
    warehouse: 1000,
    quantity: 3,
  },
  {
    id: 4,

    img: "https://cf.shopee.vn/file/3e580db475298f74211affea22d34aac_tn",
    itemName: "Nước hoa hồng diếp cá Dokudami Natural Skin Lotion 500ml",
    href: "https://shopee.vn/N%C6%B0%E1%BB%9Bc-hoa-h%E1%BB%93ng-di%E1%BA%BFp-c%C3%A1-Dokudami-Natural-Skin-Lotion-500ml-i.573427746.10365861446?xptdk=a1b8b001-4417-47f6-8e7f-98d2871f07b1",
    chosen: "White",
    price: 1244000,
    warehouse: 1000,
    quantity: 3,
  },
  {
    id: 5,

    img: "https://cf.shopee.vn/file/3e580db475298f74211affea22d34aac_tn",
    itemName: "Nước hoa hồng diếp cá Dokudami Natural Skin Lotion 500ml",
    href: "https://shopee.vn/N%C6%B0%E1%BB%9Bc-hoa-h%E1%BB%93ng-di%E1%BA%BFp-c%C3%A1-Dokudami-Natural-Skin-Lotion-500ml-i.573427746.10365861446?xptdk=a1b8b001-4417-47f6-8e7f-98d2871f07b1",
    chosen: "White",
    price: 1240500,
    warehouse: 1000,
    quantity: 3,
  },
  {
    id: 6,

    img: "https://cf.shopee.vn/file/3e580db475298f74211affea22d34aac_tn",
    itemName: "Nước hoa hồng diếp cá Dokudami Natural Skin Lotion 500ml",
    href: "https://shopee.vn/N%C6%B0%E1%BB%9Bc-hoa-h%E1%BB%93ng-di%E1%BA%BFp-c%C3%A1-Dokudami-Natural-Skin-Lotion-500ml-i.573427746.10365861446?xptdk=a1b8b001-4417-47f6-8e7f-98d2871f07b1",
    chosen: "White",
    price: 12402300,
    warehouse: 1000,
    quantity: 3,
  },
];
export const supportCategory = [
  {
    id: 1,
    title: "Mua Sắm Cùng Shopee",
    img: "https://fileproxy.scsusercontent.com/api/v2/files/c2hvcGVlLWluaG91c2UwMQ==/690d84d4103544139d22ba177c6dca97.png",
  },
  {
    id: 2,
    title: "Thanh Toán",
    img: "https://fileproxy.scsusercontent.com/api/v2/files/c2hvcGVlLWluaG91c2UwMQ==/a7c785163abc4abcbeeaefcb5bc54cc1.png",
  },
  {
    id: 3,
    title: "Trả Hàng & Hoàn Tiền",
    img: "https://fileproxy.scsusercontent.com/api/v2/files/c2hvcGVlLWluaG91c2UwMQ==/f2bc745edbfd4d7095d87da7dade354a.png",
  },
  {
    id: 4,
    title: "Thông Tin Chung",
    img: "https://fileproxy.scsusercontent.com/api/v2/files/c2hvcGVlLWluaG91c2UwMQ==/83c742e5e2cb4f108ea4ee942aa68af2.png",
  },
  {
    id: 5,
    title: "Khuyến Mãi & Ưu Đãi",
    img: "https://fileproxy.scsusercontent.com/api/v2/files/c2hvcGVlLWluaG91c2UwMQ==/c2fb3ede3472487c82131982f5ab8cbf.png",
  },
  {
    id: 6,
    title: "Đơn Hàng & Vận Chuyển",
    img: "https://fileproxy.scsusercontent.com/api/v2/files/c2hvcGVlLWluaG91c2UwMQ==/a0747397630545a7ae270dadfe73ecf5.png",
  },
  {
    id: 7,
    title: "Người Bán & Đối Tác",
    img: "https://fileproxy.scsusercontent.com/api/v2/files/c2hvcGVlLWluaG91c2UwMQ==/0ed0a957b62a4698aee7dc1df5ede7f4.png",
  },
];

export const hotQuestion = [
  {
    id: 1,
    content: "[Cảnh báo lừa đảo] Mua sắm an toàn cùng Shopee",
  },
  {
    id: 2,
    content: "[Thành viên mới] Quy trình trả hàng hoàn tiền của Shopee",
  },
  {
    id: 3,
    content:
      "[Thành viên mới] Cách theo dõi tình trạng vận chuyển của đơn hàng?",
  },
  {
    id: 4,
    content: "[Thành viên mới] Điều kiện Trả hàng/Hoàn tiền của Shopee",
  },
  {
    id: 5,
    content:
      "[Thông tin vận chuyển] Làm sao để liên hệ Đơn vị vận chuyển/tra cứu thông tin vận chuyển/hối giao hàng?",
  },
  {
    id: 6,
    content: "[Mua hàng] Các câu hỏi thường gặp trên Shopee",
  },
  {
    id: 7,
    content:
      "[Thành viên mới] Làm sao để mua hàng / đặt hàng trên ứng dụng Shopee?",
  },
  {
    id: 8,
    content:
      "[Đơn hàng] Tôi phải làm gì nếu đơn hàng bị cập nhật sai trạng thái/chưa nhận được hàng?",
  },
  {
    id: 9,
    content: "[Lỗi] Tại sao tài khoản Shopee của tôi bị khóa/bị giới hạn?",
  },
  {
    id: 10,
    content: "[Tài khoản Shopee] Tại sao tôi không nhận được Mã xác thực OTP?",
  },
];

class Address {
    constructor(id, fullName, phone, address, addressNote){
        this.id = id;
        this.fullName = fullName;
        this.phone = phone;
        this. address = address;
        this.addressNote = addressNote;
    }
}

export default Address
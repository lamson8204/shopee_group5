class Product {
    constructor(id, itemName, price, quantity, chosen, img, href, warehouse){
        this.id = id;
        this.itemName = itemName;
        this.price = price;
        this.quantity = quantity;
        this.chosen = chosen;
        this.img = img;
        this.href = href;
        this.warehouse = warehouse;
    }
}

export default Product